# A utility library detect mail server settings by a email address.

## Usage

Add denpendency in maven :

	<dependency>
    	<groupId>com.homolo</groupId>
    	<artifactId>mail-settings-detector</artifactId>
    	<version>1.0.0</version>
	</dependency>

Then you can get the settings:

	MailSettings settings = MailSettingDetector.getInstance().detect("tsc@homolo.com");
	String smtpHost = settings.getSmtpServer().getHost();
	int smtpPort = settings.getSmtpServer().getPort();
	boolean smtpSsl = settings.getSmtpServer().isSsl();
	
	String imapHost = settings.getImapServer().getHost();
	int imapPort = settings.getImapServer().getPort();
	boolean imapSsl = settings.getImapServer().isSsl();
	
	String popHost = settings.getPopServer().getHost();
	int popPort = settings.getPopServer().getPort();
	boolean popPort = settings.getPopServer().isSsl();
