/**
 * Project:mail-settings-detector
 * File:DetectorTest.java
 * Copyright 2004-2014 Homolo Co., Ltd. All rights reserved.
 */
package com.homolo.msd;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * The detector test.
 * 
 * @author rory
 */
public class DetectorTest {


    /**
     * Test the ConfigFileDetector.
     */
    @Test
    public void testConfigFileDetector() {
        ConfigFileDetector detector = new ConfigFileDetector();
        MailSettings settings = detector.detect("tester@homolo.com");
        assertNotNull(settings);

        assertEquals("smtp.ym.163.com", settings.getSmtpServer().getHost());
        assertEquals(994, settings.getSmtpServer().getPort());
        assertTrue(settings.getSmtpServer().isSsl());

        assertEquals("pop.ym.163.com", settings.getPopServer().getHost());
        assertEquals(995, settings.getPopServer().getPort());
        assertTrue(settings.getPopServer().isSsl());
        
        assertEquals("imap.ym.163.com", settings.getImapServer().getHost());
        assertEquals(993, settings.getImapServer().getPort());
        assertTrue(settings.getImapServer().isSsl());
    }

    /**
     * Test the GuessDetector.
     */
    @Test
    public void testGuessDetector() {
        GuessDetector dector = new GuessDetector();
        MailSettings guessSettings = dector.detect("tester1@homolo.com");
        assertNotNull(guessSettings);

        assertEquals("smtp.homolo.com", guessSettings.getSmtpServer().getHost());
        assertEquals(994, guessSettings.getSmtpServer().getPort());
        assertEquals(true, guessSettings.getSmtpServer().isSsl());

        assertEquals("pop.homolo.com", guessSettings.getPopServer().getHost());
        assertEquals(995, guessSettings.getPopServer().getPort());
        assertEquals(true, guessSettings.getPopServer().isSsl());

        assertEquals("imap.homolo.com", guessSettings.getImapServer().getHost());
        assertEquals(993, guessSettings.getImapServer().getPort());
        assertEquals(true, guessSettings.getImapServer().isSsl());
    }

}
