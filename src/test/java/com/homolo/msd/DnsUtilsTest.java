/**
 * Project:mail-settings-detector
 * File:DnsUtilsTest.java
 * Copyright 2004-2014 Homolo Co., Ltd. All rights reserved.
 */
package com.homolo.msd;

import static org.junit.Assert.*;

import org.junit.Test;

import com.homolo.msd.util.DnsUtils;

/**
 * The DnsUtils tester.
 * 
 * @author rory
 * @date Jul 16, 2014
 * @version $Id$
 */
public class DnsUtilsTest {

    /**
     * Test the getMXRecords.
     */
    @Test
    public void testGetMXRecords() {
        String[] records = DnsUtils.getMXRecords("homolo.com");
        assertNotNull(records);
        assertTrue(records.length > 0);
        assertEquals("mx.mail.ym.163.com", records[0]);
    }
}
