/**
 * Project:mail-settings-detector
 * File:MailSettingsDetectorTest.java
 * Copyright 2004-2014 Homolo Co., Ltd. All rights reserved.
 */
package com.homolo.msd;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * The Mail Settings Detector testor.
 *
 * @author rory
 * @version $Id$
 */
public class MailSettingsDetectorTest {


    /**
     * Test the MailSettingsDetector.
     */
    @Test
    public void testMailSettingsDetector() {
        MailSettings settings = MailSettingDetector.getInstance().detect("tester2@homolo.com");

        assertNotNull(settings);
        assertEquals("smtp.ym.163.com", settings.getSmtpServer().getHost());
        assertEquals(994, settings.getSmtpServer().getPort());
        assertTrue(settings.getSmtpServer().isSsl());

        assertEquals("imap.ym.163.com", settings.getImapServer().getHost());
        assertEquals(993, settings.getImapServer().getPort());
        assertTrue(settings.getImapServer().isSsl());

        assertEquals("pop.ym.163.com", settings.getPopServer().getHost());
        assertEquals(995, settings.getPopServer().getPort());
        assertTrue(settings.getPopServer().isSsl());
    }

    /**
     * Test the MailSettingsDetector.
     */
    @Test
    public void testMailSettingsDetector163QiYe() {
        MailSettings qiYeSettings = MailSettingDetector.getInstance().detect("tester2@lanbailawfirm.com");

        assertEquals("imap.qiye.163.com", qiYeSettings.getImapServer().getHost());
        assertEquals(993, qiYeSettings.getImapServer().getPort());
        assertTrue(qiYeSettings.getImapServer().isSsl());

        assertEquals("pop.qiye.163.com", qiYeSettings.getPopServer().getHost());
        assertEquals(995, qiYeSettings.getPopServer().getPort());
        assertTrue(qiYeSettings.getPopServer().isSsl());

        assertNotNull(qiYeSettings);
        assertEquals("smtp.qiye.163.com", qiYeSettings.getSmtpServer().getHost());
        assertEquals(994, qiYeSettings.getSmtpServer().getPort());
        assertTrue(qiYeSettings.getSmtpServer().isSsl());
    }

    /**
     * Test the MailSettingsDetector.
     */
    @Test
    public void testMailSettingsDetectorQQQiYe() {
        MailSettings qqQiYesettings = MailSettingDetector.getInstance().detect("tester2@wizlong.com");
        
        assertNotNull(qqQiYesettings);
        assertEquals("smtp.exmail.qq.com", qqQiYesettings.getSmtpServer().getHost());
        assertEquals(465, qqQiYesettings.getSmtpServer().getPort());
        assertTrue(qqQiYesettings.getSmtpServer().isSsl());
        
        assertEquals("pop.exmail.qq.com", qqQiYesettings.getPopServer().getHost());
        assertEquals(995, qqQiYesettings.getPopServer().getPort());
        assertTrue(qqQiYesettings.getPopServer().isSsl());
        
        assertEquals("imap.exmail.qq.com", qqQiYesettings.getImapServer().getHost());
        assertEquals(993, qqQiYesettings.getImapServer().getPort());
        assertTrue(qqQiYesettings.getImapServer().isSsl());
    }

    /**
     * Test domain not found settings.
     */
    @Test
    public void testMailSettingsDetectorNotFound() {
        MailSettings settings = MailSettingDetector.getInstance().detect("tester2@example.com");
        assertNull(settings);
    }

    /**
     * Test MailSettingsDetector with exception.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testMailSettingsDetectorWithException() {
        String mail1 = "tester2@com";
        MailSettingDetector.getInstance().detect(mail1);
    }

    /**
     * Test MailSettingsDetector with exception.
     */
    @Test(expected = IllegalArgumentException.class)
    public void testMailSettingsDetectorWithException2() {
        String mail2 = "tester2";
        MailSettingDetector.getInstance().detect(mail2);
    }

}
