/**
 * Project:mail-settings-detector
 * File:PortUtilsTest.java
 * Copyright 2004-2014 Homolo Co., Ltd. All rights reserved.
 */
package com.homolo.msd;

import static org.junit.Assert.*;

import org.junit.Test;

import com.homolo.msd.util.PortUtils;

/**
 * The PortUtils tester.
 * 
 * @author rory
 */
public class PortUtilsTest {

    /**
     * Test the port open.
     */
    @Test
    public void testCheckPortOpen() {
        int port80 = 80;
        String smtpHost = "smtp.homolo.com";
        assertTrue(PortUtils.isOpen("mail.homolo.com", port80));
        assertFalse(PortUtils.isOpen(smtpHost, port80));
        assertTrue(PortUtils.isOpen(smtpHost, 25));
    }
}
