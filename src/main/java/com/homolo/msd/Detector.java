package com.homolo.msd;

/**
 * The detector interface.
 *
 * @author rory
 */
public interface Detector {

    /**
     * detect the MailSettings from a mail address.
     *
     * @param mail the email address to detect
     * @return the mail settings about the email address or return null when can't detect
     */
    MailSettings detect(String mail);

}
