package com.homolo.msd;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;


/**
 * The MailSettings class for hold all Settings.
 *
 * @author rory
 */
public class MailSettings {

    /**
     * The PopServer config.
     */
    private PopServer popServer;
    /**
     * The ImapServer config.
     */
    private ImapServer imapServer;
    /**
     * The SmtpServer config.
     */
    private SmtpServer smtpServer;

    /**
     * @return the popServer
     */
    public PopServer getPopServer() {
        return popServer;
    }

    /**
     * @param popServer the popServer to set
     */
    public void setPopServer(PopServer popServer) {
        this.popServer = popServer;
    }

    /**
     * @param host the host to set
     * @param port the port to set
     * @param ssl  the ssl to set
     */
    public void setPopServer(String host, int port, boolean ssl) {
        this.popServer = new PopServer(host, port, ssl);
    }

    /**
     * @return the imapServer
     */
    public ImapServer getImapServer() {
        return imapServer;
    }

    /**
     * @param imapServer the imapServer to set
     */
    public void setImapServer(ImapServer imapServer) {
        this.imapServer = imapServer;
    }

    /**
     * @param host the host to set
     * @param port the port to set
     * @param ssl  the ssl to set
     */
    public void setImapServer(String host, int port, boolean ssl) {
        this.imapServer = new ImapServer(host, port, ssl);
    }

    /**
     * @return the smtpServer
     */
    public SmtpServer getSmtpServer() {
        return smtpServer;
    }

    /**
     * @param smtpServer the smptServer to set
     */
    public void setSmtpServer(SmtpServer smtpServer) {
        this.smtpServer = smtpServer;
    }

    /**
     * @param host the host to set
     * @param port the port to set
     * @param ssl  the ssl to set
     */
    public void setSmtpServer(String host, int port, boolean ssl) {
        this.smtpServer = new SmtpServer(host, port, ssl);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        ToStringBuilder builder = new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE);
        builder.append("pop", getPopServer()).append("imap", getImapServer()).append("smtp", getSmtpServer());
        return builder.toString();
    }

    /**
     * the abstract class for all Server.
     */
    abstract static class Server {

        /**
         * the host.
         */
        private String host;
        /**
         * the port.
         */
        private int port;
        /**
         * boolean ssl or not.
         */
        private boolean ssl;

        /**
         * @return the host
         */
        public String getHost() {
            return host;
        }

        /**
         * @param host the host to set
         */
        public void setHost(String host) {
            this.host = host;
        }

        /**
         * @return the port
         */
        public int getPort() {
            return port;
        }

        /**
         * @param port the port to set
         */
        public void setPort(int port) {
            this.port = port;
        }

        /**
         * @return is ssl
         */
        public boolean isSsl() {
            return ssl;
        }

        /**
         * @param ssl the ssl to set
         */
        public void setSsl(boolean ssl) {
            this.ssl = ssl;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String toString() {
            return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("host", getHost()).append("port", getPort()).append("ssl", isSsl()).toString();
        }
    }

    /**
     * The PopServer.
     */
    public static class PopServer extends Server {
        /**
         * The default constructor.
         */
        public PopServer() {
        }

        /**
         * The constructor with details.
         *
         * @param host the host to set
         * @param port the port to set
         * @param ssl  is ssl
         */
        public PopServer(String host, int port, boolean ssl) {
            setHost(host);
            setPort(port);
            setSsl(ssl);
        }
    }

    /**
     * The ImapServer.
     */
    public static class ImapServer extends Server {

        /**
         * The default constructor.
         */
        public ImapServer() {
        }

        /**
         * The constructor with details.
         *
         * @param host the host to set
         * @param port the port to set
         * @param ssl  is ssl
         */
        public ImapServer(String host, int port, boolean ssl) {
            setHost(host);
            setPort(port);
            setSsl(ssl);
        }
    }

    /**
     * The SmtpServer.
     */
    public static class SmtpServer extends Server {

        /**
         * The default constructor.
         */
        public SmtpServer() {
        }

        /**
         * The constructor with details.
         *
         * @param host the host to set
         * @param port the port to set
         * @param ssl  is ssl
         */
        public SmtpServer(String host, int port, boolean ssl) {
            setHost(host);
            setPort(port);
            setSsl(ssl);
        }
    }
}
