/**
 * The core package.
 * 
 * <pre>
 *  MailSettings settings = MailSettingDetector.getInstance().detect("tsc@homolo.com");
    String smtpHost = settings.getSmtpServer().getHost();
    int smtpPort = settings.getSmtpServer().getPort();
    boolean smtpSsl = settings.getSmtpServer().isSsl();
    
    String imapHost = settings.getImapServer().getHost();
    int imapPort = settings.getImapServer().getPort();
    boolean imapSsl = settings.getImapServer().isSsl();
    
    String popHost = settings.getPopServer().getHost();
    int popPort = settings.getPopServer().getPort();
    boolean popPort = settings.getPopServer().isSsl();
 * </pre>
 */
package com.homolo.msd;
